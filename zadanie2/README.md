# Radio-proxy

## Introduction

A program that connects to an internet radio server and uses the Shoutcast protocol to download audio stream and possibly metadata. The program saves separately the music and the metadata sent with it.

## Usage

Program call:

./radio-proxy parameters

Parameters:

- -h host – the name of the server providing the audio stream, mandatory;

- -r resource – the name of the resource on the server, usually only the slash, mandatory;

- -p port – the port number on which the server providing the audio stream, decimal number, mandatory;

- -m yes|no – yes, if the program should request metadata from the server, by default no, optional;

- -t timeout – time in seconds after which, if the server did not send anything, the program recognizes that the server has stopped working, default value 5 seconds, optional.
