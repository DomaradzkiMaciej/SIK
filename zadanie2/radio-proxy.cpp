#include <sys/types.h>
#include <sys/socket.h>
#include <ctime>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <cstring>
#include <cstdio>
#include <csignal>
#include <stdexcept>
#include <iostream>
#include <vector>
#include <algorithm>
#include <regex>
#include <unordered_map>
#include <thread>
#include <mutex>

#include "err.h"

#define MAX_LINE 65531
#define MAX_RADIO_NAME_LEN 8192
#define DISCOVER 1
#define IAM 2
#define KEEPALIVE 3
#define AUDIO 4
#define METADATA 6

using namespace std;

struct Client {
    sockaddr_in address;
    time_t lastActivityTime;
};

struct Communication {
    uint16_t _type;
    uint16_t _length;
    char _message[MAX_LINE];

    Communication() = default;

    Communication(uint16_t type, string &message) : _type(htons(type)), _length(message.size()) {
        if (message.size() > MAX_LINE)
            throw invalid_argument("message is too long");

        memcpy(_message, message.c_str(), message.size());
    }

    [[nodiscard]] uint16_t getType() const {
        return ntohs(_type);
    }

    [[nodiscard]] uint16_t getLength() const {
        return ntohs(_length);
    }

    [[nodiscard]] char *getMessage() {
        return _message;
    }

    void setType(uint16_t type) {
        _type = htons(type);
    }

    void setLength(uint16_t length) {
        _length = htons(length);
    }
};

struct Input {
    char *sHost = nullptr;
    char *sResource = nullptr;
    char *sPort = nullptr;
    bool sWithMeta = false;
    int sTimeout = 5;
    char *cMulti = nullptr;
    int cPort = -1;
    int cTimeout = -1;

    Input() = default;
};

int sSock = -1;
int cSock = -1;

bool withMulticast = false;
struct ip_mreq ip_mreq{};

string radioName;
unordered_map<unsigned long, Client> clients;

mutex cMutex;

static void catch_int(int sig) {
    if (close(sSock) < 0)
        syserr("close");

    if (cSock >= 0) {
        /* disconnect from a multicast group */
        if (withMulticast
            && setsockopt(cSock, IPPROTO_IP, IP_DROP_MEMBERSHIP, (void *) &ip_mreq, sizeof ip_mreq) < 0)
            syserr("setsockopt");

        if (close(cSock) < 0)
            syserr("close");
    }

    exit(0);
}

static void add_int_action() {
    struct sigaction action{};
    sigset_t block_mask;

    /* Po Ctrl-C kończymy */
    sigemptyset(&block_mask);
    action.sa_handler = catch_int;
    action.sa_mask = block_mask;
    action.sa_flags = SA_RESTART;

    if (sigaction(SIGINT, &action, nullptr) == -1)
        syserr("sigaction");
}

int get_number_from_parameter(char *parameterName, char *value) {
    int number;

    try {
        number = stoi(value);
    } catch (const std::invalid_argument &e) {
        fatal("incorrect parameter %s value - %s is not a correct number format ", parameterName, value);
    } catch (const std::out_of_range &e) {
        fatal("incorrect parameter %s value - number is too big or too small", parameterName);
    }
    if (number <= 0)
        fatal("incorrect parameter %s value - number must be positive", parameterName);

    return number;
}

Input get_arguments(int argc, char *argv[]) {
    Input input{};

    // in the argv there is ./program_name parameter value parameter value ..., so the argc must be odd
    if (argc % 2 != 1)
        fatal("incorrect parameters number");

    // get parameters
    for (int i = 1; i < argc; i += 2) {
        if (strcmp(argv[i], "-h") == 0) {
            input.sHost = argv[i + 1];
        } else if (strcmp(argv[i], "-r") == 0) {
            input.sResource = argv[i + 1];
        } else if (strcmp(argv[i], "-p") == 0) {
            input.sPort = argv[i + 1];
        } else if (strcmp(argv[i], "-m") == 0) {
            if (strcmp(argv[i + 1], "yes") == 0)
                input.sWithMeta = true;
            else if (strcmp(argv[i + 1], "no") == 0)
                input.sWithMeta = false;
            else {
                fatal("incorrect parameter -m value - %s", argv[i + 1]);
            }
        } else if (strcmp(argv[i], "-t") == 0) {
            input.sTimeout = get_number_from_parameter(argv[i], argv[i + 1]);
        } else if (strcmp(argv[i], "-P") == 0) {
            input.cPort = get_number_from_parameter(argv[i], argv[i + 1]);
        } else if (strcmp(argv[i], "-B") == 0) {
            input.cMulti = argv[i + 1];
        } else if (strcmp(argv[i], "-T") == 0) {
            input.cTimeout = get_number_from_parameter(argv[i], argv[i + 1]);
        }
    }

    // check if obligatory parameters are used
    if (input.sHost == nullptr || input.sResource == nullptr || input.sPort == nullptr)
        fatal("parameters -h, -r, -p are obligatory");

    // check if broker parameters are used correct
    if (input.cPort == -1 && (input.cMulti != nullptr || input.cTimeout != -1))
        fatal("when you use parameter -B or -T, you must use parameter -P too");

    // set withMulticast on true if multicast is used
    if (input.cMulti != nullptr)
        withMulticast = true;

    // set clients timeout
    if (input.cTimeout == -1)
        input.cTimeout = 5;

    return input;
}

void create_socket(char *remoteHost, char *remotePort, int sTimeout) {
    int err;
    struct addrinfo addr_hints{};
    struct addrinfo *addr_result;
    struct timeval tv{};

    // 'converting' host and port in string to struct addrinfo
    addr_hints.ai_family = AF_INET; // IPv4
    addr_hints.ai_socktype = SOCK_STREAM;
    addr_hints.ai_protocol = IPPROTO_TCP;

    err = getaddrinfo(remoteHost, remotePort, &addr_hints, &addr_result);
    if (err == EAI_SYSTEM) // system error
        syserr("getaddrinfo: %s", gai_strerror(err));
    else if (err != 0) // other error (host not found, etc.)
        fatal("getaddrinfo: %s", gai_strerror(err));

    // initialize socket according to getaddrinfo results
    sSock = socket(addr_result->ai_family, addr_result->ai_socktype, addr_result->ai_protocol);
    if (sSock < 0)
        syserr("socket");

    // connect socket to the server
    if (connect(sSock, addr_result->ai_addr, addr_result->ai_addrlen) < 0)
        syserr("connect");

    freeaddrinfo(addr_result);

    // add receiving timeout
    tv.tv_sec = sTimeout;
    tv.tv_usec = 0;
    if (setsockopt(sSock, SOL_SOCKET, SO_RCVTIMEO, (const char *) &tv, sizeof(tv)) < 0)
        syserr("setsockopt timeout");
}

void create_client_socket(const char *multi, int localPort) {
    int sock;
    struct sockaddr_in local_address{};

    // initialize socket
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
        syserr("socket");

    if (multi != nullptr) {
        // connect to multicast group
        ip_mreq.imr_interface.s_addr = htonl(INADDR_ANY);
        if (inet_aton(multi, &ip_mreq.imr_multiaddr) == 0) {
            syserr("ERROR: inet_aton - invalid multicast address");
        }
        if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void *) &ip_mreq, sizeof ip_mreq) < 0)
            syserr("setsockopt");
    }

    // set address and local port
    local_address.sin_family = AF_INET;
    local_address.sin_addr.s_addr = htonl(INADDR_ANY);
    local_address.sin_port = htons(localPort);
    if (bind(sock, (struct sockaddr *) &local_address, sizeof local_address) < 0)
        syserr("bind");
}

void send_request(char *sHost, char *sResource, bool sWithMeta) {
    int snprintfLen;
    char *buffer;
    size_t len;

    // get response length
    len = strlen("GET  HTTP/1.0\r\n"
                 "Host: \r\n"
                 "\r\n")
          + strlen(sHost) + strlen(sResource) + 1;

    if (sWithMeta)
        len += strlen("Icy-MetaData:1\r\n");

    buffer = new char[len];

    // write response to the buffer
    if (sWithMeta)
        snprintfLen = snprintf(buffer, len, "GET %s HTTP/1.0\r\n"
                                            "Host: %s\r\n"
                                            "Icy-MetaData:1\r\n"
                                            "\r\n",
                               sResource, sHost);
    else
        snprintfLen = snprintf(buffer, len, "GET %s HTTP/1.0\r\n"
                                            "Host: %s\r\n"
                                            "\r\n",
                               sResource, sHost);

    if (snprintfLen < 0 || snprintfLen > len)
        fatal("sprintf");

    // send response
    if (write(sSock, buffer, len - 1) != len - 1)
        syserr("partial / failed write");

    delete[] buffer;
}

// read from sSock until you get the whole header, return received data
vector<char> get_header() {
    size_t len;
    char buffer[MAX_LINE];
    const char endOfHeader[] = "\r\n\r\n";
    vector<char> response(0);

    while (true) {
        len = read(sSock, buffer, MAX_LINE);
        if (len < 0) {
            syserr("reading from server socket");
        } else if (len == 0) {
            fatal("connection closed");
        } else {
            try {
                response.insert(response.end(), buffer, buffer + len);
            } catch (exception &e) {
                fatal("insert: &s", e.what());
            }

            // check if we received whole header
            auto receivedEnd = search(response.begin(), response.end(), endOfHeader,
                                      endOfHeader + sizeof(endOfHeader) - 1);
            if (receivedEnd != response.end())
                break;
        }
    }

    return response;
}

int parse_response_header(vector<char> &response, bool sWithMeta) {
    const char endOfStatus[] = "\r\n";
    const char endOfHeader[] = "\r\n\r\n";
    auto statusEnd = search(response.begin(), response.end(), endOfStatus, endOfStatus + sizeof(endOfStatus) - 1);
    auto headerEnd = search(response.begin(), response.end(), endOfHeader, endOfHeader + sizeof(endOfHeader) - 1);

    // just in case, it should never happen
    if (statusEnd == response.end() || headerEnd == response.end())
        fatal("search");

    string status(response.begin(), statusEnd);
    string header(response.begin(), headerEnd);
    int metaint;

    // check if status is correct
    if (status != "ICY 200 OK" && status != "HTTP/1.0 200 OK" && status != "HTTP/1.1 200 OK")
        fatal("incorrect status: %s", status.data());

    // get icy-metaint value or set it to -1 if it's not in header
    try {
        regex re(R"(icy-metaint:(\d*))", regex_constants::icase);
        smatch match;

        if (std::regex_search(header, match, re) && match.size() > 1)
            metaint = stoi(match.str(1));
        else
            metaint = -1;

    } catch (regex_error &e) {
        fatal("regex: %s", e.what());
    }

    // if we din't ask for the metadata but received it, end the program
    if (!sWithMeta && metaint >= 0)
        fatal("received metaint without asking about");

    // get icy-name or set it to "" if it's not in header
    try {
        regex re(R"(icy-name:(.*))", regex_constants::icase);
        smatch match;

        if (std::regex_search(header, match, re) && match.size() > 1)
            radioName = match.str(1);
        else
            radioName = "";

    } catch (regex_error &e) {
        fatal("regex: %s", e.what());
    }

    return metaint;
}

void send_data(ostringstream &stream, int type, int timeout) {
    string messageString = stream.str();

    if (!messageString.empty()) {
        Communication message(type, messageString);
        size_t messageLen = message.getLength() + 4;

        auto addrLen = (socklen_t) sizeof(sockaddr_in);

        lock_guard<mutex> guard(mutex);

        for (auto client : clients) {
            // remove clients whose time of last activity is bigger than timeout
            if (client.second.lastActivityTime - time(nullptr) > timeout) {
                clients.erase(client.first);
                continue;
            }

            // send message
            if (sendto(cSock, &message, messageLen, 0, (struct sockaddr *) &(client.second.address), addrLen) !=
                messageLen)
                clients.erase(client.first);
        }
    }
}

void parse_response_body(const char *buffer, size_t len, int &toMetaBegin, int &toMetaEnd, int metaint,
                         ostringstream &audioStream, ostringstream &metaStream, bool withClients, int timeout) {
    // parse response byte for byte
    for (size_t i = 0; i < len; ++i) {
        if (toMetaBegin > 0 || metaint == -1) { // byte of audio
            if (metaint != -1)
                toMetaBegin -= 1;

            audioStream << buffer[i];

            if (i == len - 1) {
                if (withClients)
                    send_data(audioStream, AUDIO, timeout);
                else
                    cout << audioStream.str();

                audioStream.str("");
                audioStream.clear();
            }
        } else if (toMetaBegin == 0) { // byte of metadata length
            toMetaBegin = -1;
            toMetaEnd = 16 * buffer[i];

            if (withClients)
                send_data(audioStream, AUDIO, timeout);
            else
                cout << audioStream.str();

            audioStream.str("");
            audioStream.clear();

            if (toMetaEnd == 0)
                toMetaBegin = metaint;
        } else { // byte of metadata
            toMetaEnd -= 1;
            metaStream << buffer[i];

            if (toMetaEnd == 0) {
                if (withClients)
                    send_data(metaStream, METADATA, timeout);
                else
                    cerr << metaStream.str();

                metaStream.str("");
                metaStream.clear();

                toMetaBegin = metaint;
            }
        }
    }
}

[[noreturn]] void get_response(bool sWithMeta, bool withClients, int cTimeout) {
    int metaint;
    int toMetaBegin;
    int toMetaEnd = 0;

    const char endOfHeader[] = "\r\n\r\n";
    char buffer[MAX_LINE];
    vector<char> response(0);

    size_t len;

    ostringstream audioStream;
    ostringstream metaStream;

    response = get_header();
    metaint = parse_response_header(response, sWithMeta);
    toMetaBegin = metaint;

    // find end of header
    auto headerEnd = search(response.begin(), response.end(), endOfHeader, endOfHeader + sizeof(endOfHeader) - 1);

    // parse data received with header
    parse_response_body(&(*(headerEnd + 4)), response.end() - (headerEnd + 4), toMetaBegin, toMetaEnd,
                        metaint, audioStream, metaStream, withClients, cTimeout);

    // parse data
    while (true) {
        len = read(sSock, buffer, MAX_LINE);
        if (len < 0) {
            syserr("reading from client socket");
        } else if (len == 0) {
            fatal("connection closed");
        } else {
            parse_response_body(buffer, len, toMetaBegin, toMetaEnd, metaint, audioStream, metaStream,
                                withClients, cTimeout);
        }
    }
}

void update_time(sockaddr_in &address) {
    auto client = clients.find(address.sin_addr.s_addr);

    if (client != clients.end())
        client->second.lastActivityTime = time(nullptr);
}

[[noreturn]] void man_clients_messages() {
    Communication message{};
    sockaddr_in remote_address{};
    socklen_t addr_size;

    while (true) {
        // messages have at least 4 bytes, 2 for type and 2 for length
        if (recvfrom(cSock, &message, sizeof(Communication), 0, (struct sockaddr *) &remote_address, &addr_size)
            < 4)
            continue;

        // discard bad message
        if (message.getLength() != 0)
            continue;

        cMutex.lock();
        if (message.getType() == DISCOVER) {
            // check if client is already manned
            if (clients.find(remote_address.sin_addr.s_addr) == clients.end()) {
                clients.insert(
                        {remote_address.sin_addr.s_addr, {remote_address, time(nullptr)}});

                // send IAM message
                message.setType(IAM);
                if (snprintf(message.getMessage(), MAX_RADIO_NAME_LEN, "%s", radioName.c_str()) < 0)
                    syserr("snprintf");
                message.setLength(radioName.size());

                sendto(cSock, &message, message.getLength() + 4, 0, (struct sockaddr *) &remote_address,
                       addr_size);
            } else {
                update_time(remote_address);
            }
        } else if (message.getType() == KEEPALIVE) {
            update_time(remote_address);
        }
        cMutex.unlock();
    }
}

int main(int argc, char *argv[]) {
    add_int_action();

    auto input = get_arguments(argc, argv);

    create_socket(input.sHost, input.sPort, input.sTimeout);
    send_request(input.sHost, input.sResource, input.sWithMeta);

    if (input.cPort < 0) {
        get_response(input.sWithMeta, false, -1);
    } else {
        create_client_socket(input.cMulti, input.cPort);
        thread(man_clients_messages).detach();
        get_response(input.sWithMeta, true, input.cTimeout);
    }
}