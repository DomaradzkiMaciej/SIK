#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdio.h>

#include "err.h"

#define MAX_LINE 65535
#define MAX_COOKIE_SIZE 4096
#define MAX_HEADER_LEN 8192

int create_socket(char *host_port) {
    int sock;
    int err;
    struct addrinfo addr_hints;
    struct addrinfo *addr_result;

    // split host:port
    char *colon = strchr(host_port, ':');
    if (colon == NULL)
        fatal("incorrect host:port");
    *colon = '\0';

    char *host = host_port;
    char *port = colon + 1;

    if (host == NULL || port == NULL)
        fatal("incorrect host:port");

    // 'converting' host/port in string to struct addrinfo
    memset(&addr_hints, 0, sizeof(struct addrinfo));
    addr_hints.ai_family = AF_INET; // IPv4
    addr_hints.ai_socktype = SOCK_STREAM;
    addr_hints.ai_protocol = IPPROTO_TCP;

    err = getaddrinfo(host, port, &addr_hints, &addr_result);
    if (err == EAI_SYSTEM) // system error
        syserr("getaddrinfo: %s", gai_strerror(err));
    else if (err != 0) // other error (host not found, etc.)
        fatal("getaddrinfo: %s", gai_strerror(err));

    // initialize socket according to getaddrinfo results
    sock = socket(addr_result->ai_family, addr_result->ai_socktype, addr_result->ai_protocol);
    if (sock < 0)
        syserr("socket");

    // connect socket to the server
    if (connect(sock, addr_result->ai_addr, addr_result->ai_addrlen) < 0)
        syserr("connect");

    freeaddrinfo(addr_result);

    return sock;
}

void add_cookies_to_request(char **buffer, size_t *bufferLen, char *cookieFile) {
    char cookie[MAX_COOKIE_SIZE];
    int character;
    bool isOnlyEOFInLine;
    size_t position;
    size_t len;
    size_t headerCookieLen = strlen("Cookie: ");

    // open the file with cookies to read
    FILE *fptr = fopen(cookieFile, "r");
    if (fptr == NULL)
        syserr("fopen");

    if (fseek(fptr, 0, SEEK_END) != 0)
        fatal("fseek");

    //check if the file is empty or not.
    if (ftell(fptr) > 0) {
        rewind(fptr);

        *buffer = realloc(*buffer, sizeof(char) * ((int) (*bufferLen) + headerCookieLen + 1));
        if (*buffer == NULL)
            fatal("realloc");

        strcpy(*buffer + (*bufferLen), "Cookie: ");
        *bufferLen += headerCookieLen;

        // read file line by line
        while (true) {
            position = 0;
            isOnlyEOFInLine = true;

            // read line from the file and write it to array cookie
            while ((character = getc(fptr)) != '\n' && character != EOF) {
                cookie[position++] = character;
                isOnlyEOFInLine = false;

                if (position == MAX_COOKIE_SIZE)
                    fprintf(stderr, "ERROR: A cookie is too long, skipping rest of the cookie");
            }
            // check whether error happened during reading
            if (ferror(fptr))
                fatal("getc");

            cookie[position] = '\0';

            if (isOnlyEOFInLine == true)
                break;

            len = position + 2;
            *buffer = realloc(*buffer, sizeof(char) * ((int) (*bufferLen) + len + 1));
            if (*buffer == NULL)
                fatal("realloc");

            // write cookie to buffer
            strcpy(*buffer + (*bufferLen), cookie);
            strcpy(*buffer + (*bufferLen) + position, "; ");
            *bufferLen += len;

            if (character == EOF)
                break;
        }

        // end header with \r\n
        strcpy(*buffer + (*bufferLen) - 2, "\r\n");
    }

    if (fclose(fptr) != 0)
        syserr("fclose");
}

void get_host(char **host, char *testedHttpAddress) {
    size_t len = strlen(testedHttpAddress);
    char *address = testedHttpAddress;

    *host = malloc(sizeof(char) * (len));
    if (host == NULL)
        fatal("malloc");

    // looking for the "//"
    while (*address != '/' && *address != '\0') {
        ++address;
    }
    if (*address == '\0' || *(address + 1) != '/')
        fatal("incorrect testedHttpAddress");
    address += 2;

    // copy address without http:// or https://
    strncpy(*host, address, len - (address - testedHttpAddress));
    (*host)[len - (address - testedHttpAddress)] = '\0';
}

void send_request(int sock, char *cookieFile, char *testedHttpAddress) {
    int err;
    size_t len;
    size_t bufferLen;
    char *buffer;
    char *host;

    // truncate testedHttpAddress
    get_host(&host, testedHttpAddress);
    //split host/path
    char *slash = strchr(host, '/');
    if (slash == NULL)
        fatal("incorrect host/path");
    *slash = '\0';
    char *path = slash + 1;

    if (host == NULL || path == NULL)
        fatal("incorrect host/path");

    bufferLen = strlen("GET / HTTP/1.1\r\n"
                       "Host: \r\n") + strlen(host) + strlen(path);
    buffer = malloc(sizeof(char) * (bufferLen + 1));

    // write to buffer headers: Get and Host
    err = sprintf(buffer, "GET /%s HTTP/1.1\r\n"
                          "Host: %s\r\n", path, host);//
    if (err < 0)
        fatal("sprintf");

    add_cookies_to_request(&buffer, &bufferLen, cookieFile);

    // write to buffer header Connection
    len = strlen("Connection: close\r\n"
                 "\r\n");
    buffer = realloc(buffer, sizeof(char) * (bufferLen + len + 1));
    strcpy(buffer + bufferLen, "Connection: close\r\n"
                               "\r\n");
    bufferLen += len;

    // send request
    if (write(sock, buffer, bufferLen) != bufferLen)
        syserr("partial / failed write");

    free(host);
    free(buffer);
}

// function gets hex digit (as a char) and return it's value in dec or -1 if char is incorrect
int hex_to_int(char n) {
    if ('0' <= n && n <= '9')
        return n - '0';
    if (n == 'A' || n == 'a')
        return 10;
    if (n == 'B' || n == 'b')
        return 11;
    if (n == 'C' || n == 'c')
        return 12;
    if (n == 'D' || n == 'd')
        return 13;
    if (n == 'E' || n == 'e')
        return 14;
    if (n == 'F' || n == 'f')
        return 15;

    return -1;
}

// compare case-insensitively n characters of strings a, b and returns -1 if a<b, 0 if a=b and 1 if a>b (a,b limited
// to n characters)
int strnincmp(char const *a, char const *b, size_t n) {
    for (int i = 0; i < n; i++, a++, b++) {
        if (tolower(*a) < tolower(*b))
            return -1;
        else if (tolower(*a) > tolower(*b))
            return 1;
    }

    return 0;
}

int get_chunks_length(char *buffer, char *current, size_t *len)
{
    int chunkedLen = 0;
    int oneChunkLen;
    int decNumber;

    while(true) {
        oneChunkLen = 0;

        while (*current != '\r') {
            decNumber = hex_to_int(*current);
            if(decNumber == - 1)
                fatal("parsing chunk's length");

            oneChunkLen = 16*oneChunkLen + decNumber;
            ++current;
        }

        chunkedLen += oneChunkLen;
        if (oneChunkLen == 0){
            *len = chunkedLen;
            break;
        }

         current+= chunkedLen + 2;
    }
     return chunkedLen;
}

bool parse_response(char *buffer, size_t *len) {
    char cookie[MAX_COOKIE_SIZE];
    char status[MAX_HEADER_LEN];
    char *current = buffer;
    char *lineEnd;
    size_t headerSetCookieLen = strlen("Set-Cookie: ");
    size_t headerEncodingLen = strlen("Transfer-Encoding: chunked");
    size_t position = 0;
    bool isChunked = false;

    // check status of response
    current = strchr(current, ' ');
    ++current;

    while (*current != '\r') {
        status[position++] = *current;
        ++current;
    }

    status[position] = '\0';
    // check if the status is 200 OK and if not write report
    if (strnincmp(status, "200 OK", strlen("200 OK")) != 0) {
        fprintf(stderr, "%.*s\n", (int) position, status);
        return false;
    }

    // get current to point to next line
    current += 2;

    // iterate by response lines
    while (true) {
        lineEnd = strchr(current, '\n');
        position = 0;
        *len -= lineEnd - current + 1;

        // header ends with \r\n\r\n, so if lineEnd is second sign in line, it is an end of header
        if (current + 1 == lineEnd)
            break;

        // check if line contain cookie
        if (strnincmp(current, "Set-Cookie: ", headerSetCookieLen) == 0) {
            current += headerSetCookieLen;

            while (*current != '\r' && *current != ';') {
                cookie[position++] = *current;
                ++current;
            }

            printf("%.*s\n", (int) position, cookie);
        } else if (strnincmp(current, "Transfer-Encoding: chunked", headerEncodingLen) == 0) {
            isChunked = true;
        }

        current = ++lineEnd;
    }

    if (isChunked == true) {
        current = lineEnd + 1;
        *len = get_chunks_length(buffer, current, len);
    }

    return true;
}

void get_response(int sock) {
    bool isStatusCode200;
    size_t len;
    size_t realLength = 0;
    char buffer[MAX_LINE + 1];
    char *message = NULL;

    do {
        // read response
        len = read(sock, buffer, MAX_LINE);
        if (len < 0) {
            syserr("reading from client socket");
        } else if (len > 0) {
            // if message is NULL, it's the first time in loop, and we have to use malloc
            if (message == NULL)
                message = malloc(sizeof(char) * (len));
            else
                message = realloc(message, sizeof(char) * (realLength + len));

            if (message == NULL)
                syserr("malloc / realloc");

            // merge response
            strncpy(message + realLength, buffer, len);
            realLength += len;
        }
    } while (len > 0);

    if (message == NULL)
        fatal("didn't get anything in read");

    isStatusCode200 = parse_response(message, &realLength);
    if (isStatusCode200 == true)
        printf("Dlugosc zasobu: %ld\n", realLength);

    free(message);
}

int main(int argc, char *argv[]) {
    if (argc != 4)
        fatal("Usage: %s address:port cookieFile testedHttpAddress\n", argv[0]);

    int sock = create_socket(argv[1]);
    send_request(sock, argv[2], argv[3]);
    get_response(sock);

    if (close(sock) < 0)
        syserr("close");

    return 0;
}
