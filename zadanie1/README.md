# Testhttp

## Introduction

Programs for simple testing of websites.

The testhttp_raw program, after interpreting the command line arguments, connects to the indicated address and port, sends a request to the HTTP server to provide the indicated site, receives a response from the server, analyzes the result and reports.

The script's task is to provide testhttp_raw with the ability to test HTTPS pages.

